import { recipes } from "./recipes.js";

const allRecipes = recipes;

export function recipeList() {
    return allRecipes;
}

export function ingredientsData() {
    let allIngredients = new Set();
    for(let recipe of allRecipes) {
        for(let ingredientList of recipe.ingredients) {
            allIngredients.add('<div class="dropdown-list-item">' + ingredientList.ingredient[0].toUpperCase() + ingredientList.ingredient.substring(1) + '</div>');
        }
    }
    allIngredients = Array.from(allIngredients);
    return allIngredients.join('');
}

export function utensilsData() {
    let allUtensils = new Set();
    for(let recipe of allRecipes) {
        for(let utensilsList of recipe.utensils) {
            allUtensils.add('<li class="dropdown-list-item">' + utensilsList[0].toUpperCase() + utensilsList.substring(1) + '</li>');
        }
    }
    allUtensils = Array.from(allUtensils);
    return allUtensils.join(' \n');
}

export function appliancesData() {
    let allAppliances = new Set();
    for(let recipe of allRecipes) {
        allAppliances.add('<li class="dropdown-list-item">' + recipe.appliance[0].toUpperCase() + recipe.appliance.substring(1) + '</li>');
    }
    allAppliances = Array.from(allAppliances);
    return allAppliances.join('\n');
}
