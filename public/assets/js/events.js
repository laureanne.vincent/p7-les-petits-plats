const searchBar = document.getElementById('searchBar');
const searchButton = document.getElementById('searchButton');

const filterIngredients = document.getElementById('ingredients');
const filterUtensils = document.getElementById('utensils');
const filterAppliances = document.getElementById('appliances');
const filterIngredientsLabel = document.getElementById('ingredientsLabel');
const filterUtensilsLabel = document.getElementById('utensilsLabel');
const filterAppliancesLabel = document.getElementById('appliancesLabel');
const filterIngredientsSearch = document.getElementById('ingredientsSearch');
const filterUtensilsSearch = document.getElementById('utensilsSearch');
const filterAppliancesSearch = document.getElementById('appliancesSearch');
const filterIngredientsButton = document.getElementById('ingredientsButton');
const filterUtensilsButton = document.getElementById('utensilsButton');
const filterAppliancesButton = document.getElementById('appliancesButton');
const filterIngredientsToggle = document.getElementById('ingredientsToggle');
const filterUtensilsToggle = document.getElementById('utensilsToggle');
const filterAppliancesToggle = document.getElementById('appliancesToggle');
const filterIngredientsDropdown = document.getElementById('ingredientsDropdown');
const filterUtensilsDropdown = document.getElementById('utensilsDropdown');
const filterAppliancesDropdown = document.getElementById('appliancesDropdown');
const filterCloseButton = document.querySelector('.fa-circle-xmark');
const searchResult = document.getElementById('searchResult');
let searchQuery = '';

searchBar.addEventListener('input', () => {
    searchQuery = searchBar.value;
});

searchButton.addEventListener('click', () => {
  console.log('Query: ' + searchQuery);
});

filterIngredientsButton.addEventListener('click', () => {
  if (
    !filterIngredientsToggle.hasAttribute('style')
    && !filterIngredientsDropdown.hasAttribute('style')
    && !filterIngredientsSearch.hasAttribute('style')
  ) {
    filterIngredientsToggle.setAttribute('style', 'rotate:180deg');
    filterIngredientsDropdown.setAttribute('style', 'opacity:1');
    filterIngredientsSearch.setAttribute('style', 'display:block');
    filterIngredientsLabel.setAttribute('style', 'display:none');
    filterIngredients.setAttribute('style', 'flex-grow:0.7');
    filterIngredientsButton.style.borderRadius = '6px 6px 0 0';
  }
});

filterIngredients.addEventListener('mouseleave', () => {
  let hideDisplay = setTimeout(() => {
    filterIngredientsToggle.removeAttribute('style');
    filterIngredientsDropdown.removeAttribute('style');
    filterIngredientsSearch.removeAttribute('style');
    filterIngredientsLabel.removeAttribute('style');
    filterIngredients.removeAttribute('style');
    filterIngredientsButton.removeAttribute('style');
  }, 400);
  filterIngredients.addEventListener('mouseenter', () => {
    clearTimeout(hideDisplay);
  })
});

filterUtensilsButton.addEventListener('click', () => {
  if (
    !filterUtensilsToggle.hasAttribute('style')
    && !filterUtensilsDropdown.hasAttribute('style')
    && !filterUtensilsSearch.hasAttribute('style')
  ) {
    filterUtensilsToggle.setAttribute('style', 'rotate:180deg');
    filterUtensilsDropdown.setAttribute('style', 'opacity:1');
    filterUtensilsSearch.setAttribute('style', 'display:block');
    filterUtensilsLabel.setAttribute('style', 'display:none');
    filterUtensils.setAttribute('style', 'flex-grow:0.7');
    filterUtensilsButton.style.borderRadius = '6px 6px 0 0';
  }
});

filterUtensils.addEventListener('mouseleave', () => {
  let hideDisplay = setTimeout(() => {
    filterUtensilsToggle.removeAttribute('style');
    filterUtensilsDropdown.removeAttribute('style');
    filterUtensilsSearch.removeAttribute('style');
    filterUtensilsLabel.removeAttribute('style');
    filterUtensils.removeAttribute('style');
    filterUtensilsButton.removeAttribute('style');
  }, 400);

  filterUtensils.addEventListener('mouseenter', () => {
    clearTimeout(hideDisplay);
  })
});

filterAppliancesButton.addEventListener('click', (event) => {
  if (
    !filterAppliancesToggle.hasAttribute('style')
    && !filterAppliancesDropdown.hasAttribute('style')
    && !filterAppliancesSearch.hasAttribute('style')
  ) {
    filterAppliancesToggle.setAttribute('style', 'rotate:180deg');
    filterAppliancesDropdown.setAttribute('style', 'opacity:1');
    filterAppliancesSearch.setAttribute('style', 'display:block');
    filterAppliancesLabel.setAttribute('style', 'display:none');
    filterAppliances.setAttribute('style', 'flex-grow:0.7');
    filterAppliancesButton.style.borderRadius = '6px 6px 0 0';
  }
});

filterAppliances.addEventListener('mouseleave', () => {
  let hideDisplay = setTimeout(() => {
    filterAppliancesToggle.removeAttribute('style');
    filterAppliancesDropdown.removeAttribute('style');
    filterAppliancesSearch.removeAttribute('style');
    filterAppliancesLabel.removeAttribute('style');
    filterAppliances.removeAttribute('style');
    filterAppliancesButton.removeAttribute('style');
  }, 400);

  filterAppliances.addEventListener('mouseenter', () => {
    clearTimeout(hideDisplay);
  })

});

// filterIngredientsDropdown.addEventListener('mouseleave', (event) => {
//   filterIngredientsDropdown.removeAttribute('style');
//   filterIngredientsToggle.removeAttribute('style');
// })
//
// filterAppliancesDropdown.addEventListener('mouseleave', (event) => {
//   filterAppliancesDropdown.removeAttribute('style');
//   filterAppliancesToggle.removeAttribute('style');
// })
//
// filterUtensilsDropdown.addEventListener('mouseleave', (event) => {
//   filterUtensilsDropdown.removeAttribute('style');
//   filterUtensilsToggle.removeAttribute('style');
// })

// filterIngredientsDropdown.addEventListener('click', () => {
//   filterIngredientsDropdown.removeAttribute('style');
//   filterIngredientsToggle.removeAttribute('style');
// })
//
// filterUtensilsDropdown.addEventListener('click', () => {
//   filterUtensilsDropdown.removeAttribute('style');
//   filterUtensilsToggle.removeAttribute('style');
// })
//
// filterAppliancesDropdown.addEventListener('click', () => {
//   filterAppliancesDropdown.removeAttribute('style');
//   filterAppliancesToggle.removeAttribute('style');
// })


// TODO: fix event listener (add ID on each created component)
// filterCloseButton.addEventListener('click', () => {
//   console.log('filterCloseButton');
//   filterTag.style.display = 'none';
// });

// TODO: fix event listener (add ID on each created component)
// searchResult.addEventListener('click', () => {
//   console.log('searchResult');
// });
