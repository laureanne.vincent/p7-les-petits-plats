let filterTag = new String;
const filterTagsRow = document.getElementById('filterTagsRow');

filterTag += `
  <div class="col-2">
    <div id="filterTag" class="btn btn-warning dropdown-border-radius d-flex justify-content-between align-items-center p-3">
      <span class="me-2 small fw-bold">Coco</span>
      <i class="fas fa-xmark"></i>
    </div>
  </div>
`;

filterTagsRow.innerHTML = filterTag;
