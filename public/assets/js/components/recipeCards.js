import { recipeList } from "../searchData.js";

let allRecipes = recipeList();
let recipeCardsResult = document.getElementById('recipeCards');

let recipeCards = new String;
for(let recipe of allRecipes) {
    let allIngredients =  '';
    for (let ingredient of recipe.ingredients) {
        if (undefined === ingredient.quantity) {
            ingredient.quantity = '';
        } else {
            ingredient.quantity = ingredient.quantity;
        }

        if (undefined === ingredient.unit) {
            ingredient.unit = '';
        }

        allIngredients += `
         <div class="col-6 mb-3">
          <div class="fw-bold">${ingredient.ingredient}</div>
          <div>${ingredient.quantity} ${ingredient.unit}</div>
        </div>
        `
    }

    recipeCards += `
        <div class="col-4 mb-5">
            <div class="card h-100">
                <img src="/public/assets/media/recipe_images/${recipe.image}" class="card-img-top" alt="${recipe.name}">
                <div class="fs-6 fw-bold bg-warning rounded-pill position-absolute top-0 end-0 m-3 py-1 px-3">
                    ${recipe.time}min
                </div>
                <div class="card-body p-4 h-100">
                    <div class="fs-4 font-family-secondary mb-3">${recipe.name}</div>
                    <div class="row h-25 align-items-start mb-5">
                        <div class="col mb-3">
                            <div class="text-uppercase mb-2 fw-bold">Recette</div>
                            <div class="recipe-description">${recipe.description}</div>
                        </div>
                    </div>
                    <div class="row h-50">
                        <div class="col">
                            <div class="text-uppercase mb-2 fw-bold">Ingrédients</div>
                            <div class="row">
                                ${allIngredients}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
}


recipeCardsResult.innerHTML = recipeCards;
