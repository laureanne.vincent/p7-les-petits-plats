import {appliancesData, ingredientsData, utensilsData } from "../searchData.js";

let allAppliances = appliancesData();
let allIngredients = ingredientsData();
let allUtensils = utensilsData();

let searchCategoriesDropdown = new String;
const dropdownsRow = document.getElementById('dropdownsRow');

let categoriesHtmlContent = [{
    categoryData: allIngredients,
    categoryName: 'ingredients',
    categoryString: 'Ingrédients',
    categoryStringSingular: 'un ingrédient',
  }, {
    categoryData: allAppliances,
    categoryName: 'appliances',
    categoryString: 'Appareils',
    categoryStringSingular: 'un appareil',
  }, {
    categoryData: allUtensils,
    categoryName: 'utensils',
    categoryString: 'Ustensiles',
    categoryStringSingular: 'un ustensile',
  }
];


for(let category of categoriesHtmlContent) {
  searchCategoriesDropdown += `
    <div class="col-2 animated-growth" id="${category.categoryName}">
      <div id="${category.categoryName}Button" class="btn btn-light dropdown-border-radius d-flex justify-content-between align-items-center p-3">
        <span id="${category.categoryName}Label" class="fw-bold">${category.categoryString}</span>
          <input
            id="${category.categoryName}Search"
            class="form-control ${category.backgroundColour} border-0 search-dropdown"
            placeholder="Rechercher ${category.categoryStringSingular}"
            type="text"
          />
          <i id="${category.categoryName}Toggle" class="fa fa-chevron-down"></i>
        </div>
      </div>
      <div class="dropdown-items-wrapper">
        <div id="${category.categoryName}Dropdown" class="category-dropdown ${category.backgroundColour}">
            ${category.categoryData}
        </div>
      </div>`
  ;
}

dropdownsRow.innerHTML = searchCategoriesDropdown;
